from math import sqrt, exp, log, erf
from numpy import arange
import matplotlib.pyplot as plt
import random
import pandas as pd
import os

S0 = 100.00  # Stock price
strikes = [i for i in range(50, 150)]  # Exercise price range
T = 1  # Time to expiration
r = 0.01  # Risk free interest
q = 0.02  # Dividend yield
vol = 0.2  # Volatility
Nsteps = 100  # Nb of random Monte Carlo steps

# Cumulative distribution
def phi(x):
    return (1.0 + erf(x / sqrt(2.0))) / 2.0

# Method 1: analytical model

def blackscholes(S0, K, T, r, q, vol):
    d1 = (log(S0 / K) + (r - q + 0.5 * vol ** 2) * T) / (vol * sqrt(T))
    d2 = (log(S0 / K) + (r - q - 0.5 * vol ** 2) * T) / (vol * sqrt(T))
    value = S0 * phi(d1) * exp(-q * T) - K * exp(-r * T) * phi(d2)
    return value

# Method 2: Monte Carlo simulations
def montecarlobs (S0, K, T, r, q, vol, Nsteps):
    payoff = 0.0
    sfin = []
    for i in range(Nsteps):
        rnd = random.gauss(0, sqrt(T))
        sfinal = S0 * exp((r-q-0.5*vol**2)*T+vol*rnd)
        sfin.append(sfinal)
        payoff += max(sfinal-K,0)
    payoff = payoff * exp(-r*T) / float(Nsteps)
    return payoff

# Storing the value
AnalyticalValues = []
MontecarloValues = []

for K in strikes:
    AnalyticalValues.append(blackscholes(S0, K, T, r, q, vol))
    MontecarloValues.append(montecarlobs(S0, K, T, r, q, vol, Nsteps))

# Method 3: reading from data
datapath = os.path.abspath("../Data/BlackScholes.csv")
df_random = pd.read_csv(datapath, sep = ";")
print(df_random.head())

# Show and save the plot
fig = plt.figure(1)
plt.plot(strikes, AnalyticalValues, 'b-',
         strikes, MontecarloValues, 'ro',
         df_random['STRIKES'], df_random['OPTIONVAL'], 'g+')
plt.xlabel('Strike')
plt.ylabel('Option Value')
plt.title('European Call Option')
plt.grid(True)
plt.show()
fig.savefig(os.path.abspath("../Results/BlackScholes.png"))
